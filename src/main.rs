use std::{cmp::Ordering, collections::HashMap, fs::File, io::BufReader, path::Path};

use once_cell::sync::Lazy;
use rocket::{data::ToByteUnit, http::Status, Data};
use serde::Deserialize;
use tokio::process::Command;
use strfmt::Format;
#[macro_use] extern crate rocket;

#[derive(Deserialize)]
struct Config {
    access_token: String,
    print_command: String,
    cache_path: String,
    cache_max_files: usize,
    log_webhook: Option<String>,
}

static CONFIG: Lazy<Config> = Lazy::new(|| {
    let file = File::open("config.json").expect("No config.json found!");
    let reader = BufReader::new(file);
    serde_json::from_reader(reader).expect("config.json incorrect!")
});

fn limit_cache() {
    let mut files: Vec<std::fs::DirEntry> = std::fs::read_dir(Path::new(&CONFIG.cache_path))
        .expect("Couldn't access local directory")
        .flatten()
        .filter(|f| f.metadata().unwrap().is_file())
        .collect();
    files.sort_by(|a,b| {
        let a = a.metadata().unwrap().modified().unwrap();
        let b = b.metadata().unwrap().modified().unwrap();
        a.cmp(&b)
    });
    files.iter().rev().skip(CONFIG.cache_max_files).for_each(|f| {
        if let Err(e) = std::fs::remove_file(f.path()) {
            println!("Error removing {:?}, {:?}", f, e);
        }
    });
}

async fn print_file_unchecked(file: &str) {
    let mut fmt = HashMap::new();
    fmt.insert("image_path".to_string(), file);
    let mut child = Command::new("sh")
        .arg("-c")
        .arg(&CONFIG.print_command.format(&fmt).unwrap())
        .spawn()
        .expect("failed to spawn");

    // Await until the command completes
    let status = child.wait().await;
    println!("command finished {:?}", status);
}

#[post("/print_cache?<id>&<token>")]
async fn print_cache(id: &str, token: Option<&str>) -> Status {
    if token.unwrap_or("") != CONFIG.access_token {
        return Status::Unauthorized
    }
    let path = Path::new(&CONFIG.cache_path).join(id);
    println!("Checking path {:?}", path);
    if path.exists() {
        print_file_unchecked(path.to_str().unwrap()).await;
        Status::Ok
    } else {
        Status::NotFound
    }
}

#[post("/print?<id>&<token>", data = "<data>")]
async fn print_file<'a>(data: Data<'a>, id: &str, token: Option<&str>) -> Status {
    if token.unwrap_or("") != CONFIG.access_token {
        return Status::Unauthorized
    }
    let path = Path::new(&CONFIG.cache_path).join(id);
    println!("Using path {:?}", path);
    println!("writing data to file: {:?}", data.open(10.mebibytes()).into_file(path.clone()).await);
    if path.exists() {
        print_file_unchecked(path.to_str().unwrap()).await;
        limit_cache();
        Status::Ok
    } else {
        Status::NotFound
    }
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![print_cache, print_file])
}
